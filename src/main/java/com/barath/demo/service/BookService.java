package com.barath.demo.service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.barath.demo.dto.BookDTO;
import com.barath.demo.dto.SaveBookDTO;
import com.barath.demo.entity.Book;
import com.barath.demo.repository.BookRepository;

@Service
public class BookService {

	@Autowired
	BookRepository bookRepository;

	public BookDTO save(SaveBookDTO book) {
		Book bookToBeSaved = new Book();
		bookToBeSaved.setTitle(book.getTitle());
		bookToBeSaved.setStock(book.getStock());
		bookToBeSaved.setCreatedAt(new Date());

		Book savedBook = bookRepository.save(bookToBeSaved);

		BookDTO bookDTO = new BookDTO();
		bookDTO.setId(savedBook.getId());
		bookDTO.setTitle(savedBook.getTitle());
		bookDTO.setStock(savedBook.getStock());
		bookDTO.setCreatedAt(savedBook.getCreatedAt());

		return bookDTO;

	}

	public List<BookDTO> getBookByTitle(String title) {
		List<Book> books = bookRepository.findByTitle(title);

		List<BookDTO> booksDTO = books.stream().map((Book data) -> {
			BookDTO bookDTO = new BookDTO();
			bookDTO.setId(data.getId());
			bookDTO.setTitle(data.getTitle());
			bookDTO.setStock(data.getStock());
			bookDTO.setCreatedAt(data.getCreatedAt());
			return bookDTO;
		}).collect(Collectors.toList());

		return booksDTO;
	}

}
