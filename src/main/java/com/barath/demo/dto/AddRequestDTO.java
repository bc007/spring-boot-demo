package com.barath.demo.dto;

public class AddRequestDTO {
	private int a;
	private int b;
	
	public AddRequestDTO(int a, int b) {
		this.a = a;
		this.b = b;
	}
	public int getA() {
		return a;
	}
	public void setA(int a) {
		this.a = a;
	}
	public int getB() {
		return b;
	}
	public void setB(int b) {
		this.b = b;
	}
}
