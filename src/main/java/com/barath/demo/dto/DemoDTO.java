package com.barath.demo.dto;

public class DemoDTO {

	private String name;
	private String status;

	public DemoDTO(String name, String status) {
		this.name = name;
		this.status = status;
	}

	public String getName() {
		return this.name;
	}

	public String getStatus() {
		return this.status;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
