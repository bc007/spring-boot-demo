package com.barath.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.barath.demo.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
	List<Book> findByTitle(String title);
}
