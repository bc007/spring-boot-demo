package com.barath.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.barath.demo.dto.BookDTO;
import com.barath.demo.dto.SaveBookDTO;
import com.barath.demo.service.BookService;

@RestController
@RequestMapping("/api/v1/books")
public class BookController {
	
	@Autowired
	BookService bookService;
	
	@PostMapping
	public ResponseEntity<BookDTO> saveBooks(@RequestBody SaveBookDTO book) {
		BookDTO savedBook = bookService.save(book);
		return new ResponseEntity<BookDTO>(savedBook, HttpStatus.CREATED);
	}
	
	@GetMapping("/{title}")
	public ResponseEntity<List<BookDTO>> findBookByTitle(@PathVariable("title") String title) {
		List<BookDTO> books = bookService.getBookByTitle(title);
		return new ResponseEntity<List<BookDTO>>(books, HttpStatus.OK);
	}
}
