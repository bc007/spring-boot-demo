package com.barath.demo.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.barath.demo.dto.AddRequestDTO;
import com.barath.demo.dto.DemoDTO;

@RestController
@RequestMapping("/api/v1/demo")
public class DemoController {
	
	@GetMapping("/status")
	public ResponseEntity<DemoDTO> checkStatus() {
		DemoDTO demoDTO = new DemoDTO("Vignesh", "OK");
		ResponseEntity<DemoDTO> responseEntity = new ResponseEntity<>(demoDTO, HttpStatus.OK);
		return responseEntity;
	}
	
	@PostMapping("/add")
	public ResponseEntity<Integer> add(@RequestBody AddRequestDTO data) {
		int result = data.getA() + data.getB();
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}
}
